use inappkindb

db.projects.update({},{$set : {"projectType":"Public"}},{ multi: true });//change to Private later on
db.tapevents.update({},{$set : {"projectGuid":0}},{ multi: true });
db.taskstats.update({},{$set : {"projectGuid":0}},{ multi: true });
db.transitionbuttons.update({},{$set : {"projectGuid":0}},{ multi: true });


db.tasks.find().forEach(function(aTask) {
 	db.taskstats.find({taskGuid:aTask.guid}).forEach(function(aTaskStat)
 	{
 		db.taskstats.update({guid:aTaskStat.guid},{ $set: { projectGuid:aTask.projectGuid } });
 		
 	});
});

db.sketches.find({}).forEach(function(aSketch){

 	db.transitionbuttons.find({sketchGuid:aSketch.guid}).forEach(function(aTransButton)
 	{
 		db.transitionbuttons.update({guid:aTransButton.guid},{ $set: { projectGuid:aSketch.projectGuid } });
 		
 	});
 	
 	db.tapevents.find({sketchGuid:aSketch.guid}).forEach(function(aTapEvent)
 	{
 		db.tapevents.update({guid:aTapEvent.guid},{ $set: { projectGuid:aSketch.projectGuid }});
 	});
});

db.transitionbuttons.remove({projectGuid:0});