var config = require('./config'),
    restify = require('restify'),
    mongoose = require('mongoose'),
    async = require('async'),
    package = require('./package.json'),
    logger=config.logger;

/**
 * Our own exit codes. Have to begin after exit codes defined by node
 * @see https://nodejs.org/api/process.html#process_exit_codes
 */
var exitCodes = Object.freeze({
    MONGO_CONNECT_FAILED: 20
});

//Create Server
var server = restify.createServer({
    name: 'iNappkin-api'
});
server.listen(config.port, function () {
    logger.info('%s %s listening at %s', server.name, config.version, server.url, {});
});
server.use(restify.bodyParser());
server.use(restify.fullResponse());

var db = mongoose.connect(config.mongoURL, function(err) {
    if (err) {
        logger.error("failed to connect to mongo", err);
        process.exit(exitCodes.MONGO_CONNECT_FAILED);
    }
});

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var User = require(__dirname+'/models/'+'User.js');
var Task = require(__dirname+'/models/'+'Task.js');
var TaskStat= require(__dirname+'/models/'+'TaskStat.js');
var TapEvent = require(__dirname+'/models/'+'TapEvent.js');
var TransitionButton = require(__dirname+'/models/'+'TransitionButton.js');
var Sketch = require(__dirname+'/models/'+'Sketch.js');
var Project = require(__dirname+'/models/'+'Project.js');
var Subscription = require(__dirname+'/models/'+'Subscription.js');

var UserDoc = mongoose.model('User');
var TaskDoc = mongoose.model('Task');
var TaskStatDoc= mongoose.model('TaskStat');
var TapEventDoc = mongoose.model('TapEvent');
var TransitionButtonDoc = mongoose.model('TransitionButton');
var SketchDoc = mongoose.model('Sketch');
var ProjectDoc = db.model('Project');
var SubscriptionDoc = db.model('Subscription');
var testerUser="--iNappkin Tester--";

server.get('/version', function (req, res, next) {
    res.contentType = 'text/plain';
    res.send(package.version);
});

//Sync Projects
server.post('sync/:tableName', function (req, res, next) {
        var numberOfItems=Object.keys(req.body).length;
        var maxLast;
        var userName,deviceId;
        var docVar;
        var fileName;
        var projectFlag=false;
        switch(req.params.tableName) {
            case "Project":
                docName=ProjectDoc;
                fileName=Project;
                projectFlag=true;
                break;
            case "Sketch":
                docName=SketchDoc;
                fileName=Sketch;
                break;
            case "Task":
                docName=TaskDoc;
                fileName=Task;
                break;
            case "TaskStat":
                docName=TaskStatDoc;
                fileName=TaskStat;
                break;
            case "TapEvent":
                docName=TapEventDoc;
                fileName=TapEvent;
                break;
            case "TransitionButton":
                docName=TransitionButtonDoc;
                fileName=TransitionButton;
                break;
            default:
                logger.error('Error: request parameter is not in the correct form, request must contain one of the entities names');
                return next(new restify.InvalidArgumentError('Error: request parameter is not in the correct form, request must contain one of the entities names'));
        }
        async.eachSeries(Object.keys(req.body),function(itemKey,callbackAsync) {
            item = req.body[itemKey];
            if(itemKey == 'userName')
            {
                userName=req.body[itemKey];
                numberOfItems--;
                if(numberOfItems==0)
                    sendDocsToClient(projectFlag,userName, deviceId,docName, maxLast,res);
                callbackAsync();
            } 
            else if(itemKey == 'deviceIdentifier')
            {
                deviceId=req.body[itemKey];
                numberOfItems--;
                if(numberOfItems==0)
                    sendDocsToClient(projectFlag,userName, deviceId,docName, maxLast,res);
                callbackAsync();
            }
            else if(itemKey == 'MaxLastModified')
            {
                dateitem = req.body[itemKey];
                maxLast=dateitem;
                numberOfItems--;
                if(numberOfItems==0)
                    sendDocsToClient(projectFlag,userName, deviceId,docName, maxLast,res);
                callbackAsync();
            }
            else
            {
                docName.find({ guid: item.guid },function (error, resultData)
                {    
                    if(resultData.length==0)//record is new
                    {
                            var tempDoc = fileName.createInstance(item);
                            tempDoc.save(function (error, data)
                            {
                                if (error) 
                                {
                                    logger.error('Error occured while saving in collection: ' + docName.modelName + '. Error: '+ error);
                                    return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
                                }
                                numberOfItems--;
                                if(numberOfItems == 0)
                                    sendDocsToClient(projectFlag,userName, deviceId,docName, maxLast,res);
                                callbackAsync();
                            });   
                    }
                    else if (resultData.length == 1)
                    {
                        fileName.updateDocument(item, function (error, data) {
                                        if (error) 
                                        {
                                            logger.error('Error occured while updating in collection: ' + docName.modelName + '. Error: '+ error);
                                            return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
                                        }
                                        numberOfItems--;
                                        if(numberOfItems == 0)
                                            sendDocsToClient(projectFlag,userName, deviceId,docName, maxLast,res);
                                        callbackAsync();
                            });
                    }
                    else
                    {
                        logger.error('Database error, maltiple documents have the same guid');
                        return next(new restify.InvalidArgumentError('Database error, maltiple documents have the same guid'));
                        
                    }
                });
            }
         // tell async that the iterator has complete
        }, function(err) {
            if(err)
            {
	            logger.error('eachSync error, please check if async is inlcuded in the server code. Error: ' + err);
	            return next(new restify.InvalidArgumentError('eachSync error, please check if async is inlcuded in the server code. Error: ' + err));
            }
        });  
        
});

server.post('/subscribe', function (req, res, next) {
		console.log(req.body.deviceIdentifier);
        console.log(req.body.projectGuid);
        
        SubscriptionDoc.count({$and: [{deviceIdentifier: req.body.deviceIdentifier},{projectGuid: req.body.projectGuid}]},function(err, count){
        if(err)
        {
            logger.error('Error occured while subscibing to project ' + req.body.projectGuid + '. Error: ' + error);
            return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
        }
        if(count==0)
        { 
	        var subscriptionData = new SubscriptionDoc({
	                                        deviceIdentifier: req.body.deviceIdentifier,
	                                        projectGuid: req.body.projectGuid
	                                    });
	        var SubscriptionObject = new SubscriptionDoc(subscriptionData);
	        SubscriptionObject.save(function (error, savedData)
	        {
	            if (error) 
                {
                   logger.error('Cannot subscribe to project ' + req.body.projectGuid+ '. Error: ' + error);
                   return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
                }
                logger.log('device ID '+req.body.deviceIdentifier+' has subscribed to project '+ req.body.deviceIdentifier.projectGuid);
            	res.send(savedData);
	        }); 
        }
        else
        {
            logger.log('device ID '+req.body.deviceIdentifier+' has subscribed to project '+ req.body.deviceIdentifier.projectGuid);
            res.send({"Subscription": "Done"});
        }
    });
});

function sendDocsToClient(objType, userName, deviceId,docs,maxLast,res) {
        maxLast= new Date(maxLast);
        if(userName == testerUser)//request came from tester device
        {
            //get the public project and and projects of deviceId
            SubscriptionDoc.find({deviceIdentifier: deviceId},{ _id:0, projectGuid:1 },
             function(err, privateProjects){
                	var prvGuids=[];
                 	privateProjects.forEach(function(item) { 
                    	    prvGuids.push(item.projectGuid);
                	});
                	ProjectDoc.find({"projectType": "Public"},{guid:1,_id:0},function(err, publicGuids){  //get the pulic guids or the guids belong to the user
                    var pubGuids=[];
                	publicGuids.forEach(function(item) { 
                        pubGuids.push(item.guid);
                    });
                 	var allGuids=[];
                  	logger.log('info: public IDs:',pubGuids);
                  	logger.log('info: private IDs:',prvGuids);
                	if(objType)
                    {
                        docs.find(
                        {$or: [{$and: [{"last_modified" : { $gt : maxLast } },
                        {guid: {$in: pubGuids}}]},
                        {guid: {$in: prvGuids}}]}
	                    ,
	                    function(err, data) 
	                    {
	                        if (err) 
	                        {
	                            logger.error('Unable to find data in ' + docs.modelName + '. Error:'+err);
	                            return next(new restify.InvalidArgumentError(JSON.stringify(err.errors)));
	                        }
	                        res.send(data);
	                        logger.log(docs.modelName, + ' have been sent to '+ userName);
	                    });
                 	}
                	else
                    {
	                    docs.find(
	                        {$or: [{$and: [{"last_modified" : { $gt : maxLast } },
	                        {projectGuid: {$in: pubGuids}} ] },
	                        {projectGuid: {$in: prvGuids}}]}
			            	,
			                function(err, data) 
			                {
		                        if (err) 
		                        {
		                            logger.error('Unable to find data in ' + docs.modelName + '. Error:' + err);
		                            return next(new restify.InvalidArgumentError(JSON.stringify(err.errors)));
		                        }
		                        res.send(data);
		                        logger.log(docs.modelName, + ' have been sent to '+ userName);
                    		});
                    }
                });
            });
        }
        else //request came from designer
        {
            ProjectDoc.find({ $or: [{"projectType": "Public"},{"userName":userName}]},{guid:1,_id:0},function(err, publicGuids)
            {  //get the pulic guids or the guids belong to the user
            	var array=[];
                publicGuids.forEach(function(item) { 
                    array.push(item.guid);
                });
                var fieldName;
                if(objType)
                {
                    docs.find(
          	        	{$and: [{"last_modified" : { $gt : maxLast } },
            	        {guid: {$in: array}} ] }
                		,
                		function(err, data) 
                		{
                        	if (err) 
                        	{
                            	logger.error('Unable to find data in '+docs.modelName+'. Error:'+err);
                            	return next(new restify.InvalidArgumentError(JSON.stringify(err.errors)));
                        	}
                        	res.send(data);
                        	logger.log(docs.modelName, + ' have been sent to '+ userName);
                    	});
                }
                else
                {
                    docs.find(
                    	{$and: [{"last_modified" : { $gt : maxLast } },
                    	{projectGuid: {$in: array}} ] }
                		,
                		function(err, data)
                		{
                        	if (err) 
                        	{
                            	logger.error('Unable to find data in '+docs.modelName+'. Error:'+err);
                            	return next(new restify.InvalidArgumentError(JSON.stringify(err.errors)));
                        	}
                        	res.send(data);
                        	logger.log(docs.modelName, + ' have been sent to '+ userName);
                    	});
                }
                
            });
            
        }
        logger.log('info','response sent');
}

//users call
server.get('/user/:uid', function (req, res, next) {
    UserDoc.find({ userName: req.params.uid},
    function(err, data)
    {
        if (err)
        {
            logger.error('There is a problem finding user:' + req.params.uid+ '. Error: ' + err);
            return next(new restify.InternalError(JSON.stringify(err.errors)));
        }
        res.send(data);
    });
});

//post
server.post('/user', function (req, res, next) {
    if (req.body.userName === undefined) {
        return next(new restify.InvalidArgumentError('userName must be supplied'));
    }
    if (req.body.password === undefined) {
        return next(new restify.InvalidArgumentError('password must be supplied'));
    }
    var userData = {
        userName:req.body.userName,
        password: req.body.password
    };
    UserDoc.count({userName: req.body.userName},
    function(err, data)
    {
        if(data>0)
        {
            logger.error('Username already exists: ' + req.params.uid);
            res.send({error: 'Username already exists!'});
            return;
        }   
        var user = new UserDoc(userData);
        user.save(function (error, data) 
        {
            if (error) 
            {
                logger.error('Unable to save in users. Error: ' + error);
                return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
            } 
            else 
            {
                res.send(data);
            }
        })
    });
});

//delete
server.del('/user', function (req, res, next) {
    UserDoc.remove({   
    }, function (error, user) {
        if (error) 
        {
            logger.error('Unable to delete in users. Error: ' + error);
            return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
        }
     })
});