group "inappkin" do
  action :create
end

user "inappkin" do
  gid "inappkin"
  system true
  shell "/bin/bash"
  action :create
end

directory "/home/inappkin" do
  owner 'inappkin'
  group 'inappkin'
  mode '0700'
  action :create
end

execute "global-node-packages" do
  command "npm install -g forever@0.11.1"
  not_if { ::File.exists?("/usr/bin/forever") }
end

directory "/opt" do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

directory "/opt/inapsrv" do
  owner 'inappkin'
  group 'inappkin'
  mode '6775'
  action :create
end

directory "/var/opt/inapsrv" do
  owner 'inappkin'
  group 'inappkin'
  mode '6755'
  action :create
end

directory "/var/opt/inapsrv/log" do
  owner 'inappkin'
  group 'inappkin'
  mode '6755'
  action :create
end

cookbook_file "/opt/inapsrv/start.sh" do
  source "start.sh"
  owner 'root'
  group 'root'
  mode '0744'
  action :create
end

cookbook_file "/opt/inapsrv/stop.sh" do
  source "stop.sh"
  owner 'root'
  group 'root'
  mode '0744'
  action :create
end

cookbook_file "/etc/init.d/inapsrv" do
  source "inapsrv"
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

service "inapsrv" do
  supports :status => false, :restart => true
  action [:enable]
end