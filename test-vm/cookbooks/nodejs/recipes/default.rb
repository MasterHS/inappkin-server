remote_file "#{Chef::Config[:file_cache_path]}/epel-release-6-8.noarch.rpm" do
    source "http://ftp-stud.hs-esslingen.de/pub/epel/6/i386/epel-release-6-8.noarch.rpm"
    action :create
end

rpm_package "epel" do
    source "#{Chef::Config[:file_cache_path]}/epel-release-6-8.noarch.rpm"
    action :install
end

package "nodejs"
package "npm"
