cookbook_file "/etc/yum.repos.d/mongodb.repo" do
  source "etc/yum.repos.d/mongodb.repo"
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

package "mongodb-org"

cookbook_file "/etc/mongod.conf" do
  source "etc/mongod.conf"
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

service "mongod" do
  action [:enable, :restart]
end
