'use strict';

var program = require('commander'),
    packageJson = require('./package.json'),
    winston = require('winston');

program
    .version(packageJson.version)
    .option('-l, --log [file]', 'Log to file')
    .option('-L, --log-level [level]', 'Log level threshold, defaults to info')
    .parse(process.argv);

var logLevel = (program.logLevel && program.logLevel !== true) ? program.logLevel : 'info';

var logConfig = {
    timestamp: function() {
        return new Date().toISOString();
    },
    formatter: function(options) {
        // Return string will be passed to logger.
        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (undefined !== options.message ? options.message : '') +
            (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
    },
    level: logLevel,
    filename: program.log,
    json: false,
    tailable: true,
    maxFiles: 10,
    maxsize: 10485760 //10MB
};

var logTransport = (program.log && program.log !== true) ?
        new (winston.transports.File)(logConfig) :
        new (winston.transports.Console)(logConfig);

// Extend a winston by making it expand errors when passed in as the
// second argument (the first argument is the log level).
function expandErrors(logger) {
    var oldLogFunc = logger.log;
    logger.log = function() {
        var args = Array.prototype.slice.call(arguments, 0);
        if (args.length >= 1 && args[0] == 'error') {
            var pos = 1;
            while (pos<args.length) {
                if  (args[pos] instanceof Error) {
                    args[pos] = '\n' + args[pos].stack;
                    break;
                }
                pos++;
            }
        }
        return oldLogFunc.apply(this, args);
    };
    return logger;
}

var logger = expandErrors(new (winston.Logger)({
    transports: [logTransport]
}));

module.exports = {
    port: process.env.INAPPKIN_PORT ? process.env.INAPPKIN_PORT : 7000,
    mongoURL: process.env.INAPPKIN_MONGO_HOST ? process.env.INAPPKIN_MONGO_URL : 'mongodb://localhost/inappkindb',
    version: packageJson.version,
    logger: logger
};

logger.debug('using config %j', module.exports, {});
