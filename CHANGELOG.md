<a name"3.2.0"></a>
## 3.2.0 (2015-06-20)


<a name"3.1.1"></a>
### 3.1.1 (2015-06-02)


<a name"3.1.0"></a>
## 3.1.0 (2015-05-09)


#### Features

* **app.js:** use logger instead of console for start info (fcdc1e66)
* **deploy:** use log files upon forever start (b3ee86a7)


<a name"3.0.0"></a>
## 3.0.0 (2015-05-09)


#### Bug Fixes

* **app.js:** removing unused modules (309841f7)


#### Features

* **app:** add logger and make it configurable via command line options (6580cfb8)
* **server:** add generic sync and test it with project request (bd374ff3)


<a name"2.0.1"></a>
### 2.0.1 (2015-04-28)


#### Features

* **deployment:** add basic deployment process based on `grunt-ssh-deploy` (6876aa64)
* **test-vm:** add an vagrant and chef based virtual machine for testing deployment process (f1c3472d)


<a name"2.0.0"></a>
## 2.0.0 (2015-04-28)


<a name="1.0.0"></a>
## 1.0.0 (2015-03-22)


#### Features

* **app.js:**
  * add version to the starting log entry and /version to the rest api ((258280a2))
  * allow to configure the server via environment variables ((37872d74))


