module.exports = function (grunt) {
    'use strict';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            all: ['dist/', 'tmp/']
        },
        asciidoctor: {
            options: {
                doctype: 'article',
                backend: 'html5',
                header_footer: true
            },
            tmp: ['*.adoc']
        },
        copy: {
            asciidoctor: {
                files: [
                    {expand: true, src: ['asciidoctor.css'], dest: 'tmp/', cwd: 'asciidoctor'}
                ]
            },
            dist: {
                files: [
                    {
                        expand: false,
                        src: ['**/*.js', '!Gruntfile.js', '!node_modules/**/*.js', '!dist/**/*.js', 'package.json'],
                        dest: 'dist/'
                    }
                ]
            }
        },
        inline: {
            asciidoctor: {
                options:{
                    cssmin: true,
                    tag: ''
                },
                expand: true,
                cwd: 'tmp/',
                src: '*.html',
                dest: 'dist/'
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                '**/*.js'
            ]
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: ['pkg'],
                commitFiles: ['package.json', 'CHANGELOG.md'],
                commitMessage: 'chore(release): release %VERSION%',
                tagName: '%VERSION%',
                push: false
            }
        },
        changelog: {
            options: {
                file: 'CHANGELOG.md'
            }
        },
        secret: grunt.file.readJSON('secret.json'),
        environments: {
            options: {
                local_path: 'dist',
                current_symlink: 'current',
                deploy_path: '/opt/inapsrv',
                before_deploy: 'cd /opt/inapsrv/current && forever stopall'
            },
            vagrant: {
                options: {
                    host: '<%= secret.vagrant.host %>',
                    username: '<%= secret.vagrant.username %>',
                    password: '<%= secret.vagrant.password %>',
                    port: '<%= secret.vagrant.port %>',
                    debug: true,
                    number_of_releases: '2',
                    after_deploy: 'cd /opt/inapsrv/current && npm install --production && forever start app.js --log-level debug'
                }
            },
            staging: {
                options: {
                    host: '<%= secret.staging.host %>',
                    username: '<%= secret.staging.username %>',
                    password: '<%= secret.staging.password %>',
                    port: '<%= secret.staging.port %>',
                    debug: true,
                    number_of_releases: '2',
                    after_deploy: 'cd /opt/inapsrv/current && npm install --production && forever start app.js --log /var/opt/inapsrv/log/inapsrv.log --log-level debug'
                }
            },
            production: {
                options: {
                    host: '<%= secret.production.host %>',
                    username: '<%= secret.production.username %>',
                    password: '<%= secret.production.password %>',
                    port: '<%= secret.production.port %>',
                    number_of_releases: '5',
                    after_deploy: 'cd /opt/inapsrv/current && npm install --production && forever start app.js --log /var/opt/inapsrv/log/inapsrv.log'
                }
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('release', function(target) {
        if (target !== 'patch' && target !== 'minor' && target !== 'major' && target !== 'prerelease') {
            grunt.fatal('not a valid target!');
        }
        grunt.task.run(['bump-only:'+target, 'changelog', 'bump-commit']);
    });
    grunt.registerTask('docs', ['clean', 'copy:asciidoctor', 'asciidoctor', 'inline:asciidoctor']);
    grunt.registerTask('default', ['docs']);

    grunt.registerTask('deploy', function(target) {
        if (target !== 'staging' && target !== 'production' && target !== 'vagrant') {
            grunt.fatal('not a valid target!');
        }
        grunt.task.run(['clean', 'copy:dist', 'ssh_deploy:'+target]);
    });
};
