var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = Schema({
    guid: {
        type: String,
        required: true,
        trim: true
    },
    projectGuid: {
        type: String,
        required: true,
        trim: true
    },
    last_modified: {
        type: Date,
        true: true
    },
    is_deleted: {
        type: String,
        required: true,
        trim: true
    },
    taskName: {
        type: String,
        required: true,
        trim: true
    },
    taskDescription: {
        type: String,
        required: false,
        trim: true
    },
    firstSketchGuid: {
        type: String,
        required: true,
        trim: true
    },
    destinationSketchGuid: {
        type: String,
        required: true,
        trim: true
    }
});

mongoose.model('Task', TaskSchema);

module.exports = {
  createInstance: function (item) {
    var Task = mongoose.model('Task');
    var task=new Task({
                                last_modified: new Date(),
                                is_deleted: item.is_deleted,
                                guid: item.guid,
                                projectGuid:item.projectGuid,
                                taskName: item.taskName,
                                taskDescription: item.taskDescription,
                                firstSketchGuid: item.firstSketchGuid,
                                destinationSketchGuid: item.destinationSketchGuid
                            });
    return task;
  
  },
  updateDocument: function (item,callback) {
    var Document = mongoose.model('Task');
    Document.update({guid: item.guid},{
                                            last_modified: new Date(),
                                            is_deleted: item.is_deleted,
                                            projectGuid:item.projectGuid,
                                            taskName: item.taskName,
                                            taskDescription: item.taskDescription,
                                            firstSketchGuid: item.firstSketchGuid,
                                            destinationSketchGuid: item.destinationSketchGuid
                                     }

                    ,function (error, data) {
                    if(error)
                    {
                        callback(true, "Unable to update Task")
                    }
                    callback(null, "Task Updated!")
                });
  }

};