var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProjectSchema = Schema({
    guid: {
        type: String,
        required: false,
        trim: true
    },
    last_modified: {
        type: Date,
        trim: true
    },
    is_deleted: {
        type: String,
        required: false,
        trim: true
    },
    deviceId: {
        type: String,
        required: false,
        trim: true
    },
    projectName: {
        type: String,
        required: false,
        trim: true
    },
    projectDescription: {
        type: String,
        required: false,
        trim: true
    },
    deviceType: {
        type: String,
        required: false,
        trim: true
    },
    deviceOrientation: {
        type: String,
        required: false,
        trim: true
    },
    projectType: {
        type: String,
        required: false,
        trim: true
    },
    userName: {
        type: String,
        required: false,
        trim: true
    }

});

mongoose.model('Project', ProjectSchema);

module.exports = {
  createInstance: function (item) {
    var Project = mongoose.model('Project');
    var project=new Project({
                                last_modified: new Date(),
                                is_deleted: item.is_deleted,
                                guid: item.guid,
                                projectName: item.projectName,
                                deviceOrientation: item.deviceOrientation,
                                projectDescription: item.projectDescription,
                                projectType: item.projectType,
                                deviceId: item.deviceId,
                                deviceType: item.deviceType,
                                userName : item.userName
                            });

    return project;
  },
  updateDocument: function (item,callback) {
    var Project = mongoose.model('Project');
    Project.update({guid: item.guid}, {
                    last_modified: new Date(),
                    is_deleted: item.is_deleted,
                    deviceId:item.deviceId,
                    projectName:item.projectName,
                    projectDescription:item.projectDescription,
                    deviceType: item.deviceType,
                    projectType: item.projectType,
                    deviceOrientation: item.deviceOrientation,
                    userName:item.userName 
                    },function (error, data) {
                    if(error)
                    {
                        callback(true, "Unable to update Project")
                    }
                    callback(null, "Project Updated!")
                });
  }
};