var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SketchSchema = Schema({
    guid: {
        type: String,
        required: true,
        trim: true
    },
    projectGuid: {
        type: String,
        required: true,
        trim: true
    },
    last_modified: {
        type: Date,
        trim: true
    },
    is_deleted: {
        type: String,
        required: true,
        trim: true
    },
    sketchOrderNumber:{
        type: Number,
        required: true
    },
    imageFile: {
        type: String,
        required: false,
        trim: true
    },
    uploaded_new: {
        type: String,
        required: false,
        trim: true
    }
});

mongoose.model('Sketch', SketchSchema);

module.exports = {
  createInstance: function (item) {
    var Sketch = mongoose.model('Sketch');
    var sketch=new Sketch({
                                last_modified: new Date(),
                                is_deleted: item.is_deleted,
                                guid: item.guid,
                                imageFile: item.imageFile,
                                projectGuid:item.projectGuid,
                                uploaded_new:item.uploaded_new,
                                sketchOrderNumber: item.sketchOrderNumber
                            });


    return sketch;
   },
  updateDocument: function (item,callback) {
    var Document = mongoose.model('Sketch');
    Document.update({guid: item.guid},
    {
                                            last_modified: new Date(),
                                            is_deleted: item.is_deleted,
                                            projectGuid: item.projectGuid,
                                            uploaded_new:item.uploaded_new,
                                            //imageFile: item.imageFile,
                                            sketchOrderNumber: item.sketchOrderNumber
                                     }

                    ,function (error, data) {
                    if(error)
                    {
                        callback(true, "Unable to update Sketch")
                    }
                    callback(null, "Sketch Updated!")
                });
  }


};