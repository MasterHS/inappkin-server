var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema=Schema({
    userName: {
        unique: true,
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    }

});

mongoose.model('User', UserSchema);