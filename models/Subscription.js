var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var SubscriptionSchema=Schema({
    deviceIdentifier: {
        type: String,
        required: true,
        trim: true
    },
    projectGuid: {
        type: String,
        required: true,
        trim: true
    }
});

mongoose.model('Subscription', SubscriptionSchema);