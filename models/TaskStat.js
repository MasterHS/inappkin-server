var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskStatSchema = Schema({
    guid: {
        type: String,
        required: true,
        trim: true
    },
    taskGuid: {
        type: String,
        required: true,
        trim: true
    },
    last_modified: {
        type: Date,
        trim: true
    },
    is_deleted: {
        type: String,
        required: true,
        trim: true
    },
    projectGuid: {
        type: String,
        required: true,
        trim: true
    },
    deviceIdentifier:{
        type: String,
        required: false,
        trim: true
    },
    taskCompletionTime: {
        type: String,
        required: false,
        trim: true
    },
    numberOfMistakes: {
        type: String,
        required: false,
        trim: true
    },
    numberOfRepetitions: {
        type: String,
        required: false,
        trim: true
    },
    timeToLearn: {
        type: String,
        required: false,
        default: Date.now
    },
    taskCompleted: {
        type: String,
        required: false,
        trim: true
    },
    timeSpentOnErrors: {
        type: String,
        required: false,
        default: Date.now
    }
});

mongoose.model('TaskStat', TaskStatSchema);

module.exports = {
  createInstance: function (item) {
    var TaskStat = mongoose.model('TaskStat');
    var taskStat=new TaskStat({
                                last_modified: new Date(),
                                is_deleted: item.is_deleted,
                                guid: item.guid,
                                taskGuid: item.taskGuid,
                                deviceIdentifier: item.deviceIdentifier,
                                projectGuid:item.projectGuid,
                                taskCompletionTime: item.taskCompletionTime,
                                taskCompleted: item.taskCompleted
                            });
    return taskStat;
  },
  updateDocument: function (item,callback) {
    var Document = mongoose.model('TaskStat');
    Document.update({guid: item.guid},{
                                            last_modified: new Date(),
                                            is_deleted: item.is_deleted,
                                            guid: item.guid,
                                            taskGuid: item.taskGuid,
                                            projectGuid:item.projectGuid,
                                            deviceIdentifier: item.deviceIdentifier,
                                            taskCompletionTime: item.taskCompletionTime,
                                            taskCompleted: item.taskCompleted
                                     }

                    ,function (error, data) {
                    if(error)
                    {
                        callback(true, "Unable to update TaskStat")
                    }
                    callback(null, "TaskStat Updated!")
                });
  }

};