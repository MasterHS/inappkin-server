var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var db = mongoose.connect("mongodb://localhost/projects");

var TransitionButtonSchema = Schema({
    guid: {
        type: String,
        required: true,
        trim: true
    },
    sketchGuid: {
        type: String,
        required: true,
        trim: true
    },
    last_modified: {
        type: Date,
        trim: true
    },
    projectGuid: {
        type: String,
        required: true,
        trim: true
    },
    is_deleted: {
        type: String,
        required: true,
        trim: true
    },
    xAxis: {
        type: String,
        required: true,
        trim: true
    },
    yAxis: {
        type: String,
        required: true,
        trim: true
    },
    width: {
        type: String,
        required: true,
        trim: true
    },
    height: {
        type: String,
        required: true,
        trim: true
    },
    destinationSketchGuid: {
        type: String,
        required: false,
        trim: true
    }
});

mongoose.model('TransitionButton', TransitionButtonSchema);

module.exports = {
  createInstance: function (item) {
    var TransitionButton = mongoose.model('TransitionButton');
    var transButton=new TransitionButton({
                                last_modified: new Date(),
                                is_deleted: item.is_deleted,
                                guid: item.guid,
                                sketchGuid: item.sketchGuid,
                                xAxis: item.xAxis,
                                projectGuid: item.projectGuid,
                                yAxis: item.yAxis,
                                width: item.width,
                                height: item.height,
                                destinationSketchGuid: item.destinationSketchGuid
                            });
    return transButton;
  
    
  },
  updateDocument: function (item,callback) {
    var Document = mongoose.model('TransitionButton');
    Document.update({guid: item.guid},{
                                            last_modified: new Date(),
                                            is_deleted: item.is_deleted,
                                            sketchGuid: item.sketchGuid,
                                            xAxis: item.xAxis,
                                            yAxis: item.yAxis,
                                            projectGuid:item.projectGuid,
                                            width: item.width,
                                            height: item.height,
                                            destinationSketchGuid: item.destinationSketchGuid
                                     }

                    ,function (error, data) {
                    if(error)
                    {
                        callback(true, "Unable to update TransitionButton")
                    }
                    callback(null, "TransitionButton Updated!")
                });
  }

};