var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TapEventSchema = Schema({
    guid: {
        type: String,
        required: true,
        trim: true
    },
    last_modified: {
        type: Date,
        trim: true
    },
    is_deleted: {
        type: String,
        required: true,
        trim: true
    },
    projectGuid: {
        type: String,
        required: true,
        trim: true
    },
    xAxis: {
        type: String,
        required: true,
        trim: true
    },
    yAxis: {
        type: String,
        required: true,
        trim: true
    },
    taskGuid: {
        type: String,
        required: true,
        trim: true,
        expose: false
    },
    deviceType: {
        type: String,
        required: false,
        trim: true,
        expose: false
    },
    sketchGuid: {
        type: String,
        required: true,
        trim: true,
        expose: false
    },
    timeOnSketch:{
        type: String,
        required: true,
        trim: true
    }
});

mongoose.model('TapEvent', TapEventSchema);

module.exports = {
  createInstance: function (item) {
    var TapEvent = mongoose.model('TapEvent');
    var tapEvent=new TapEvent({
                                last_modified: new Date(),
                                is_deleted: item.is_deleted,
                                guid: item.guid,
                                sketchGuid: item.sketchGuid,
                                taskGuid: item.taskGuid,
                                timeOnSketch: item.timeOnSketch,
                                projectGuid:item.projectGuid,
                                xAxis: item.xAxis,
                                yAxis: item.yAxis,
                                deviceType: item.deviceType
                            });
    return tapEvent;
  },
  updateDocument: function (item,callback) {
    var Document = mongoose.model('TapEvent');
    Document.update({guid: item.guid},
                    {
                                            last_modified: new Date(),
                                            is_deleted: item.is_deleted,
                                            sketchGuid: item.sketchGuid,
                                            taskGuid: item.taskGuid,
                                            timeOnSketch: item.timeOnSketch,
                                            projectGuid:item.projectGuid,
                                            xAxis: item.xAxis,
                                            yAxis: item.yAxis,
                                            deviceType: item.deviceType
                                     }
                    ,function (error, data) {
                    if(error)
                    {
                        callback(true, "Unable to update TapEvent")
                    }
                    callback(null, "TapEvent Updated!")
                });
  }
  

};